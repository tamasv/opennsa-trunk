Installing OpenNSA on Debian 7.0


1) Update and upgrade debia:

apt-get update && apt-get upgrade


2) Install the following packages/applications:

apt-get install -y git

3) Install PostgreSQL Server and its development dependencies:

apt-get install -y postgresql postgresql-plpython-9.1 postgresql-server-dev-9.1

4) Install Python 2.7 (CentOS depends of Python2.6 for its package system)

apt-get install -y python python-minimal python-pip python-dev python-openssl


5-10) Use pip to install deps
root@OpenNSA:/usr/src/opennsa-trunk-gts# pip install -r docs/requirements-for-pip.txt 

11) Initialize and Start the PostgreSQL

/etc/init.d/postgresql restart

12) Prepare the environment for Opennsa

useradd -m opennsa
mkdir /usr/src/opennsa
chown opennsa:opennsa /usr/src/opennsa

13) Install OpenNSA

su - opennsa -c "cd /usr/src/opennsa
git clone -b nsi2-r99 git://git.nordu.net/opennsa.git /usr/src/opennsa
cd /usr/src/opennsa
git checkout nsi2
python2.7 setup.py build
exit
"
 
14) Create the database

cp datafiles/schema.sql /tmp/
su - postgres -c "
createdb opennsa
createuser -RSD opennsa
"
su - opennsa -c "
psql opennsa -c '\i /tmp/schema.sql'
"

15) It's important to keep the server time accurate (NTP)

apt-get install ntp
/etc/init.d/ntpd start

16) Generate your SSH keys

su - opennsa -c"
ssh-keygen -t rsa -N '' -f ~/.ssh/opennsa_rsa.key
"
 
17) Edit these two configuration files accordingly to your environment (under /home/opennsa/opennsa):
 # vi opennsa.conf

 [service]
 network=<YOUR_NETWORK_NAME>
 logfile=
 nrmmap=opennsa.nrm
 host=<SERVER_FQDN>
 database=opennsa
 dbuser=opennsa
 dbpassword=<YOUR_PASS>
 tls=false

 # vi opennsa.nrm (your topology)

 For topology configuration, please take a look at the example configuration

18) Create a ".opennsa-cli" file under ~opennsa/

 echo -e "bandwidth=200\nhost=localhost\nport=7080\nstarttime=+1\nendtime=+20" > ~opennsa/.opennsa-cli
 
 The starttime and the endtime represent when the circuit will start and end in seconds

19) Configure your backend in the opennsa.conf

 # OpenNSA has support for the following backends: brocade, dell, etc...
 # So create a section for your backend with the following format:
 #
 [backend_type]
 host=x.x.x.x
 user=opennsa
 fingerprint=xx:xx:xx:xx:xx:xx:xx...
 publickey=/home/opennsa/.ssh/id_rsa.pub
 privatekey=/home/opennsa/.ssh/id_rsa

 # if your backend is Brocade, an example:
 #
 [brocade]
 host=x.x.x.x
 user=opennsa
 fingerprint=xx:xx:xx:xx:xx:xx:xx....
 publickey=/home/opennsa/.ssh/id_rsa.pub
 privatekey=/home/opennsa/.ssh/id_rsa
 enablepassword=XXXXX

20) Start the OpenNSA:

 su - opennsa
 cd opennsa
 twistd -ny opennsa.tac
 (-n to not create a daemon. There is also an init.d script)

 You should see:
 
 2013-07-02 14:17:08-0400 [-] Log opened.
 2013-07-02 14:17:08-0400 [-] twistd 13.1.0 (/usr/local/bin/python2.7 2.7.3) starting up.
 2013-07-02 14:17:08-0400 [-] reactor class: twisted.internet.epollreactor.EPollReactor.
 2013-07-02 14:17:08-0400 [-] OpenNSA service initializing
 2013-07-02 14:17:08-0400 [-] Provider URL: http://<SERVER_FQDN>:9080/NSI/services/CS2
 2013-07-02 14:17:08-0400 [-] Topology URL: http://<SERVER_FQDN>:9080/NSI/topology/<YOUR_NETWORK>.xml
 2013-07-02 14:17:08-0400 [-] Site starting on 9080
 2013-07-02 14:17:08-0400 [-] Starting factory <twisted.web.server.Siteinstance at 0x1df39e0>
 2013-07-02 14:17:08-0400 [-] OpenNSA service started

21) Configure your network device to support authentication using SSH keys

 In general, you have to copy your public key (id_dsa.pub) to the network device
 and insert this key in the valid users key chain. Check your device's documentation to confirm how to make this  
 configuration.
 
 As an example, in Brocade MLX switches you have to:
 
 a. cd ~opennsa/.ssh
 b. echo "---- BEGIN SSH2 PUBLIC KEY ----" > keys.txt
 c. cat id_dsa.pub >> keys.txt
 d. echo "---- END SSH2 PUBLIC KEY ----" >> keys.txt
 e. Remove the encryption algorithm (ssh-rsa):  sed -i 's/ssh-rsa //'keys.txt
 d. Remove the user (opennsa@<SERVER_FQDN>): sed -i 's/opennsa@<SERVER_FQDN>//' keys.txt
 f. Upload the keys.txt to your TFTP server
 g. Log into your Brocade device and:
 g1. SSH@Brocade# configure terminal
 g2. SSH@Brocade(config)# ip ssh pub-key-file tftp <TFTP_SERVER> keys.txt
 g3. SSH@Brocade(config)# ip ssh key-authentication yes
 g4. SSH@Brocade(config)# end
 g5. SSH@Brocade# write memory

22) Now you are ready to run OpenNSA

 Use the document docs/usage for understand how to use it (under development).

References:
 
 https://github.com/NORDUnet/opennsa/blob/nsi2/INSTALL


