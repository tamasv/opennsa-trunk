import StringIO

from twisted.trial import unittest

from opennsa.topology import nrm


NRM_ENTRY = \
"""
# some comment
ethernet     ps              -                               vlan:1780-1788  1000    em0     -
ethernet     netherlight     netherlight#nordunet-(in|out)   vlan:1780-1783  1000    em1     -
ethernet     somelight       netherlight#somelight-(in|out)  vlan:1780-1780  1000    em8     -
ethernet     uvalight        uvalight#uvalight-(in|out)      vlan:1780-1783  1000    em2     -
ethernet     otherlight      otherlight#uvalight(-in|-out)   vlan:1780-1785  1000    em5     -
ethernet     gtslight        netherlight#gtslight-(in|out)   mpls            1000    em6     -
ethernet     gtslight2       netherlight#gtslight2-(in|out)  port            1000    em9     -
"""


class NRMParserTest(unittest.TestCase):

    def testPortMapping(self):

        nrm_ports = nrm.parsePortSpec( StringIO.StringIO(NRM_ENTRY) )

        port_map = dict( [ (p.name, p.interface) for p in nrm_ports ] )

        self.assertEquals( port_map.get('ps'),           'em0')
        self.assertEquals( port_map.get('netherlight'),  'em1')
        self.assertEquals( port_map.get('somelight'),    'em8')
        self.assertEquals( port_map.get('uvalight'),     'em2')
        self.assertEquals( port_map.get('otherlight'),   'em5')
        self.assertEquals( port_map.get('gtslight'),     'em6')
        self.assertEquals( port_map.get('gtslight2'),    'em9')

        # should test alias as well
    
    def testRemotePort(self):

        nrm_ports = nrm.parsePortSpec( StringIO.StringIO(NRM_ENTRY) )

        port_map = dict( [ (p.name, p.remote_port) for p in nrm_ports ] )

        self.assertEquals( port_map.get('netherlight'), 'netherlight:nordunet')
        self.assertEquals( port_map.get('somelight'),   'netherlight:somelight')
        self.assertEquals( port_map.get('uvalight'),    'uvalight:uvalight')
        self.assertEquals( port_map.get('otherlight'),  'otherlight:uvalight')
        self.assertEquals( port_map.get('gtslight'),    'netherlight:gtslight')
        self.assertEquals( port_map.get('gtslight2'),   'netherlight:gtslight2')

    def testRemoteInOut(self):

        nrm_ports = nrm.parsePortSpec( StringIO.StringIO(NRM_ENTRY) )

        port_map = dict( [ (p.name, p) for p in nrm_ports ] )

        self.assertEquals( port_map.get('netherlight').remote_in,   'netherlight:nordunet-in')
        self.assertEquals( port_map.get('somelight').remote_in,     'netherlight:somelight-in')
        self.assertEquals( port_map.get('uvalight').remote_in,      'uvalight:uvalight-in')
        self.assertEquals( port_map.get('otherlight').remote_in,    'otherlight:uvalight-in')
        self.assertEquals( port_map.get('gtslight').remote_in,      'netherlight:gtslight-in')
        self.assertEquals( port_map.get('gtslight2').remote_in,     'netherlight:gtslight2-in')

        self.assertEquals( port_map.get('netherlight').remote_out,   'netherlight:nordunet-out')
        self.assertEquals( port_map.get('somelight').remote_out,     'netherlight:somelight-out')
        self.assertEquals( port_map.get('uvalight').remote_out,      'uvalight:uvalight-out')
        self.assertEquals( port_map.get('otherlight').remote_out,    'otherlight:uvalight-out')
        self.assertEquals( port_map.get('gtslight').remote_out,      'netherlight:gtslight-out')
        self.assertEquals( port_map.get('gtslight2').remote_out,     'netherlight:gtslight2-out')


